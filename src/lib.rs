#![doc=include_str!("../README.md")]
#![doc(html_logo_url = "https://gitlab.com/john_t/geoclue-zbus/-/raw/master/logo.png")]

use serde_repr::{Deserialize_repr, Serialize_repr};
use zbus::proxy;
use zbus::zvariant::{OwnedValue, Type, Value};

/// A type to indicate that the value is in meters.
pub type Meters = f64;

/// A type to indicate that a value is in degrees.
pub type Degrees = f64;

/// Speed in meters per second.
pub type Speed = f64;

/// Used to specify level of accuracy requested by, or allowed
/// for a client.
#[derive(Serialize_repr, Deserialize_repr, PartialEq, Debug, Type, OwnedValue, Value)]
#[repr(u32)]
pub enum AccuracyLevel {
    None = 0,
    Country = 1,
    City = 2,
    Neighborhood = 3,
    Srteet = 4,
    Exact = 5,
}

/// The Location interface
///
/// This is the interface you use on location objects.
#[proxy(
    interface = "org.freedesktop.GeoClue2.Location",
    default_service = "org.freedesktop.GeoClue2"
)]
trait Location {
    /// Accuracy property
    #[zbus(property)]
    fn accuracy(&self) -> zbus::Result<Meters>;

    /// Altitude property.
    ///
    /// When unknown, it's set to [`f64::MIN`].
    #[zbus(property)]
    fn altitude(&self) -> zbus::Result<Meters>;

    /// Description property
    ///
    /// WARNING: Applications should not rely on this property
    /// since not all sources provide a description. If you
    /// really need a description (or more details) about current
    /// location, use a reverse-geocoding API, e.g. geocode-glib.
    #[zbus(property)]
    fn description(&self) -> zbus::Result<String>;

    /// Heading property.
    ///
    /// The heading direction in degrees with respect to North
    /// direction, in clockwise order. That means North becomes
    /// 0 degree, East: 90 degrees, South: 180 degrees, West: 270
    /// degrees and so on. When unknown, it's set to -1.0.
    #[zbus(property)]
    fn heading(&self) -> zbus::Result<Degrees>;

    /// Latitude property
    #[zbus(property)]
    fn latitude(&self) -> zbus::Result<Degrees>;

    /// Longitude property
    #[zbus(property)]
    fn longitude(&self) -> zbus::Result<Degrees>;

    /// Speed property
    #[zbus(property)]
    fn speed(&self) -> zbus::Result<Speed>;

    /// Timestamp property
    ///
    /// These are the seconds and microseconds since the Epoch. It
    /// is probably best to convert it to a [`std::time::SystemTime`]
    #[zbus(property)]
    fn timestamp(&self) -> zbus::Result<(u64, u64)>;
}

/// The Application-specific client API
///
/// This is the interface you use to retrieve location information
/// and receive location update signals from GeoClue service.
/// You get the client object to use this interface on
/// [`ManagerProxy::get_client`] method.
#[proxy(
    interface = "org.freedesktop.GeoClue2.Client",
    default_service = "org.freedesktop.GeoClue2"
)]
trait Client {
    /// Start receiving events about the current location.
    /// Applications should hook up to
    /// [`ClientProxy::receive_location_updated`] signal
    /// before calling this method.

    fn start(&self) -> zbus::Result<()>;

    /// Stop receiving events about the current location.
    fn stop(&self) -> zbus::Result<()>;

    /// The signal is emitted every time the location changes.
    /// The client should set the `distance_threshold` property to
    /// control how often this signal is emitted.
    ///
    /// * old: old location as path to a [`LocationProxy`]
    ///
    /// * new: new location as path to a [`LocationProxy`]
    #[zbus(signal)]
    fn location_updated(
        &self,
        old: zbus::zvariant::ObjectPath<'_>,
        new: zbus::zvariant::ObjectPath<'_>,
    ) -> zbus::Result<()>;

    /// Active property
    #[zbus(property)]
    fn active(&self) -> zbus::Result<bool>;

    /// DesktopId property
    ///
    /// The desktop file id (the basename of the desktop file).
    /// This property must be set by applications for
    /// authorization to work.
    #[zbus(property)]
    fn desktop_id(&self) -> zbus::Result<String>;

    /// Sets DesktopId property
    ///
    /// The desktop file id (the basename of the desktop file).
    /// This property must be set by applications for
    /// authorization to work.
    #[zbus(property)]
    fn set_desktop_id(&self, value: &str) -> zbus::Result<()>;

    /// DistanceThreshold property
    #[zbus(property)]
    fn distance_threshold(&self) -> zbus::Result<u32>;
    #[zbus(property)]
    fn set_distance_threshold(&self, value: u32) -> zbus::Result<()>;

    /// Current location as path to a
    /// [`LocationProxy`]
    /// object. Please note that this property will be set to
    /// "/" (D-Bus equivalent of null) initially, until Geoclue
    /// finds user's location. You want to delay reading this
    /// property until your callback to
    /// [`ClientProxy::receive_location_updated`] signal
    /// is called for the first time after starting the client.
    #[zbus(property, object = "Location")]
    fn location(&self) -> zbus::Result<zbus::zvariant::OwnedObjectPath>;

    /// RequestedAccuracyLevel property
    #[zbus(property)]
    fn requested_accuracy_level(&self) -> zbus::Result<AccuracyLevel>;

    #[dbus_proxy(property)]
    fn set_requested_accuracy_level(&self, value: AccuracyLevel) -> zbus::Result<()>;

    /// TimeThreshold property
    ///
    /// Contains the current time threshold in seconds. This
    /// value is used by the service when it gets new location
    /// info. If the time since the last update is below the
    /// threshold, it won't emit the LocationUpdated signal.
    /// The default value is 0. When TimeThreshold is zero, it
    /// always emits the signal.
    #[zbus(property)]
    fn time_threshold(&self) -> zbus::Result<u32>;
    /// Sets the TimeThreshold property
    ///
    /// Sets the current time threshold in seconds. This
    /// value is used by the service when it gets new location
    /// info. If the time since the last update is below the
    /// threshold, it won't emit the LocationUpdated signal.
    /// The default value is 0. When TimeThreshold is zero, it
    /// always emits the signal.
    #[zbus(property)]
    fn set_time_threshold(&self, value: u32) -> zbus::Result<()>;
}

/// This is the interface you use to talk to main GeoClue2
/// manager object at path "/org/freedesktop/GeoClue2/Manager".
/// The only thing you do with this interface is to call
/// [`ManagerProxy::get_client()`] or [`ManagerProxy::create_client`]
/// on it to get your application specific client object(s).
#[proxy(
    interface = "org.freedesktop.GeoClue2.Manager",
    default_path = "/org/freedesktop/GeoClue2/Manager",
    default_service = "org.freedesktop.GeoClue2"
)]
trait Manager {
    /// AddAgent method
    ///
    /// An API for user authorization agents to register themselves.
    /// Each agent is responsible for the user it is running as.
    /// Application developers can and should simply ignore
    /// this API.
    fn add_agent(&self, id: &str) -> zbus::Result<()>;

    /// CreateClient method
    ///
    /// Creates and retrieves a client object which can only be
    /// used by the calling application only. Unlike
    /// [`ManagerProxy::get_client`], this method always creates a
    /// new client.
    #[zbus(object = "Client")]
    fn create_client(&self);

    /// DeleteClient method
    ///
    /// Use this method to explicitly destroy a client, created
    /// using [`ManagerProxy::get_client`] or
    /// [`ManagerProxy::create_client`].
    ///
    /// Long-running applications, should either use this to
    /// delete associated client(s) when not needed, or
    /// disconnect from the D-Bus connection used for
    /// communicating with Geoclue (which is implicit on client
    /// process termination).
    fn delete_client(&self, client: &zbus::zvariant::ObjectPath<'_>) -> zbus::Result<()>;

    /// GetClient method
    ///
    /// Retrieves a client object which can only be used by the
    /// calling application only. On the first call from a
    /// specific D-Bus peer, this method will create the client
    /// object but subsequent calls will return the path of the
    /// existing client.
    #[zbus(object = "Client")]
    fn get_client(&self);

    /// AvailableAccuracyLevel property
    #[zbus(property)]
    fn available_accuracy_level(&self) -> zbus::Result<AccuracyLevel>;

    /// InUse property
    ///
    /// Whether service is currently is use by any application.
    #[zbus(property)]
    fn in_use(&self) -> zbus::Result<bool>;
}
